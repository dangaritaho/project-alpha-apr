from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm


@login_required
def list_projects(request):
    view_project = Project.objects.filter(owner=request.user)
    context = {
        "project_instance_list": view_project,
    }
    return render(request, "projects/list.html", context)


@login_required
def show_project(request, id):
    show_project_details = get_object_or_404(Project, id=id)
    detail_task = get_object_or_404(Project, id=id).tasks.all()
    context = {
        "project": show_project_details,
        "detail": detail_task,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
